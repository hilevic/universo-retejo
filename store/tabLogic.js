export default {
  namespaced: true,
  state() {
    return {
      leftDrawer: true,
      currentTab: 0,
      tabs: [
        {
          id: 0,
          name: "Реальный",
          component: {
            template: "<div>Реальный</div>",
          },
          backgroundTabColor: "rgba(102, 153, 254, 0.35)",
          imageFull: "/icons/tab2_1.png",
          imageMin: "/icons/tab4_2.png",
          wideFull: 205,
          wideMin: 48,
          min: false,
          imageStyle: 0,
        },
        {
          id: 1,
          name: "Конкордо",
          component: {
            template: "<div>Конкордо</div>",
          },
          backgroundTabColor: "rgba(83, 0, 17, 0.35)",
          imageFull: "/icons/tab1_1.png",
          imageMin: "/icons/tab4_1.png",
          wideFull: 205,
          wideMin: 48,
          min: true,
          imageStyle: 0,
        },
        {
          id: 2,
          name: "Триумфо",
          component: {
            template: "<div>Триумфо</div>",
          },
          backgroundTabColor: "rgba(0, 101, 101, 0.35)",
          imageFull: "/icons/tab3_1.png",
          imageMin: "/icons/tab4_3.png",
          wideFull: 205,
          wideMin: 48,
          min: true,
          imageStyle: 0,
        },
      ],
    };
  },
  getters: {
    imageStyle: (state) => (i) => {
      return `background-image: url( ${
        !state.tabs[i].min ? state.tabs[i].imageFull : state.tabs[i].imageMin
      }); width: ${
        !state.tabs[i].min ? state.tabs[i].wideFull : state.tabs[i].wideMin
      }px;`;
    },
    leftDrawer: (state) => (width) => {
      return width < 1260 ? state.leftDrawer : true;
    },
    backgroundColor: (state) => {
      return state.tabs[state.currentTab].backgroundTabColor;
    },
  },
  mutations: {
    clickOnTab(state, tab) {
      if (state.currentTab === tab) {
        state.tabs[tab].min = !state.tabs[tab].min;
      } else {
        state.currentTab = tab;
        state.tabs[tab].min = false;

        for (let i = 0; i < state.tabs.length; i++) {
          i !== state.currentTab ? (state.tabs[i].min = true) : "";
        }
      }
    },
    leftDrawer(state, width) {
      width < 1260 ? (state.leftDrawer = !state.leftDrawer) : "";
    },
  },
  computed: {},
};
